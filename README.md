# ft_printf
![Project illustration](./illustration.png "DIY")<br>
This project is the 3rd of the 42 School cursus.

# Description
In this project, we must reimplement a lite version of the printf function from the C standard library.<br>
Our own printf function must be able to manage:<br>
- the following flags:<br>
  - `#`<br>
  - `0`<br>
  - `-`<br>
  - `+`<br>
  - ` ` (space)<br>
- the field width<br>
- the precision<br>
- the following length modifiers:<br>
  - `hh`<br>
  - `h`<br>
  - `l`<br>
  - `ll`<br>
- the following conversions:<br>
  - `c`<br>
  - `s`<br>
  - `p`<br>
  - `d`<br>
  - `i`<br>
  - `u`<br>
  - `x`<br>
  - `X`<br>
  - `%`<br>
<br>
